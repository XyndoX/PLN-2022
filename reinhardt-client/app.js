const App = {
    data(){
        return {
            fruechte: [],
            newName: "Banane",
            newAnzahl: "12",
            drogen: [],
            newBeschriftung: "LSD",
            newMasse: "12g",
            newDealer: "MaxMakerMax"
        };
    },
    methods:
    {
        getAllDrogen(){
            console.log("Drogenballern")
            fetch('/drogen/')
            .then(response => response.json())
            .then(data => this.drogen = data)
        },
        createNewDroge(){
            const newDroge = { beschriftung: this.newBeschriftung, masse: this.newMasse, dealer: this.newDealer};
            fetch('/drogen/', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(newDroge)
            })
        },

        getAllFruechte(){
            console.log("Daten Geholt!")
            fetch('/fruechte/')
            .then(response => response.json())
            .then(data => this.fruechte = data)
        },

        createNewFrucht(){
            const newFrucht = { name: this.newName, anzahl: this.newAnzahl};
            fetch('/fruechte/', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(newFrucht)
            })
        },
        buttonNewClick(){
          this.createNewFrucht();
          this.getAllFruechte()  
        },
        buttonNewDroge(){
            this.createNewDroge();
            this.getAllDrogen()
        }
    

    
    },

    mounted(){
        this.getAllFruechte();
        this.getAllDrogen();
    }

};

Vue.createApp(App).mount('#app');