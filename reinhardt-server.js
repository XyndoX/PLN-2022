import express from 'express';
import { randomUUID } from 'crypto';

const PORT = 12345;

const fruechte = [
    { id: '1', name: 'Apfel', anzahl: '2'},
    { id: '2', name: 'Kohnlefrucht', anzahl: '4' },
    { id: '3', name: 'Arme Ananas', anzahl: '187' }
];
const drogen = [
    { id: '1', beschriftung: 'Jibit', masse: '2g', dealer: 'MakerSnacks'},
    { id: '2', beschriftung: 'Plieing', masse: '4g', dealer: 'MakerSnacks' },
    { id: '3', beschriftung: 'Neccos rechter hoden', masse: '2', dealer: 'MakerSnacks' }
];

//---------------------------
// Webserver
//---------------------------
const app = express();
app.use(express.json()); // Für POST: request.body

//-----------------------------------------------
// GET / -> statische Dateien aus public-Ordner
//-----------------------------------------------
app.use(express.static('reinhardt-client'));

//---------------------------
// GET /fruechte/
//---------------------------
app.get('/fruechte/', (request, response) => {
    response.send(fruechte);
});

//---------------------------
// GET /fruechte/{id}
//---------------------------
app.get('/fruechte/:id', (request, response) => {
    const id = request.params.id;
    const gesuchteFrucht = fruechte.find(frucht => frucht.id === id);
    if(gesuchteFrucht){
        // passende Frucht gefunden
        response.send(gesuchteFrucht);
    }else{
        // keine passende Frucht gefunden
        response.sendStatus(404);
    }    
});

//------------------------------------------
// POST /fruechte/ (neue Frucht erstellen)
//------------------------------------------
app.post('/fruechte/', (request, response) => {
    const neueFrucht = request.body;
    neueFrucht.id = randomUUID();
    console.log(neueFrucht);
    fruechte.push(neueFrucht);
    fruechte.sort(
        (frucht1, frucht2) => frucht1.anzahl.localeCompare(frucht2.anzahl)
    );
    response.sendStatus(200);

});

//------------------------------------------
// DELETE /fruechte/{id} (Frucht löschen)
//------------------------------------------
app.delete('/fruechte/:id', (request, response) => {
    const id = request.params.id;
    const index = fruechte.findIndex(frucht => frucht.id === id);
    if(index >= 0){
        // passende Frucht gefunden
        // Frucht löschen
        fruechte.splice(index, 1);
        response.sendStatus(204);
    }else{
        // keine passende Frucht gefunden
        response.sendStatus(404);
    }    
});



///////////////////////////////////////////////////////////////////////////////////////////////////////////
//---------------------------
// GET /drogen/
//---------------------------
app.get('/drogen/', (request, response) => {
    response.send(drogen);
});

//---------------------------
// GET /drogen/{id}
//---------------------------
app.get('/drogen/:id', (request, response) => {
    const id = request.params.id;
    const gesuchteDroge = drogen.find(droge => droge.id === id);
    if(gesuchteDroge){
        // passende Frucht gefunden
        response.send(gesuchteDroge);
    }else{
        // keine passende Frucht gefunden
        response.sendStatus(404);
    }    
});

//------------------------------------------
// POST /drogen/ (neue Droge erstellen)
//------------------------------------------
app.post('/drogen/', (request, response) => {
    const neueDroge = request.body;
    neueDroge.id = randomUUID();
    console.log(neueDroge);
    drogen.push(neueDroge);
    drogen.sort(
        (droge1, droge2) => droge1.beschriftung.localeCompare(droge2.beschriftung)
    );
    response.sendStatus(200);

});

//------------------------------------------
// DELETE /fruechte/{id} (Frucht löschen)
//------------------------------------------
app.delete('/droge/:id', (request, response) => {
    const id = request.params.id;
    const index = drogen.findIndex(droge => droge.id === id);
    if(index >= 0){
        // passende Frucht gefunden
        // Frucht löschen
        droge.splice(index, 1);
        response.sendStatus(204);
    }else{
        // keine passende Frucht gefunden
        response.sendStatus(404);
    }    
});

//---------------------------
// Webserver starten
//---------------------------
app.listen(PORT, () => {
    console.log(`Server läuft auf Port ${PORT}.`);
});